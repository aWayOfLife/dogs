package com.example.dogs.view


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.dogs.R
import com.example.dogs.model.DogBreed
import com.example.dogs.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {

    private lateinit var viewModel: ListViewModel
    private  val dogsListAdapter = DogsListAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.refresh()

        dog_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = dogsListAdapter
        }
        refresh_layout.setOnRefreshListener {
            dog_recycler_view.visibility=View.GONE
            list_error.visibility=View.GONE
            list_progress_bar.visibility=View.VISIBLE
            viewModel.refreshBypassCache()
            refresh_layout.isRefreshing=false
        }

        observeViewModel()
    }

    private fun observeViewModel(){

        viewModel.loading.observe(this, Observer { isLoading ->
            isLoading?.let{
                list_progress_bar.visibility = if(it) View.VISIBLE else View.GONE
                if(it){
                    list_error.visibility = View.GONE
                    dog_recycler_view.visibility = View.GONE
                }
            }
        })

        viewModel.dogsLoadError.observe(this, Observer { isError ->
            isError?.let{
                list_error.visibility = if(it) View.VISIBLE else View.GONE
            }
        })

        viewModel.dogs.observe(this, Observer { dogsList ->
            dogsList?.let{
                dog_recycler_view.visibility =View.VISIBLE
                dogsListAdapter.updateDogsList(dogsList)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_settings ->{
                view?.let { Navigation.findNavController(it).navigate(ListFragmentDirections.actionListFragmentToSettingsFragment()) }

            }
        }
        return super.onOptionsItemSelected(item)
    }
}
