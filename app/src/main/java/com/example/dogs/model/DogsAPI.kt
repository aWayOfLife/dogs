package com.example.dogs.model

import io.reactivex.Single
import retrofit2.http.GET

interface DogsAPI {
    @GET ("DevTides/DogsApi/master/dogs.json")
    fun getdogs(): Single<List<DogBreed>>
}